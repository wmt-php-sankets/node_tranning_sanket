'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const User = use('App/Models/User')
const Blog = use('App/Models/Blog')
const { validate } = use('Validator')

/**
 * Resourceful controller for interacting with blogs
 */
class BlogController {
  /**
   * Show a list of all blogs.
   * GET blogs
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async displayallblog ({ request, response, view }) {
    try{
      const allBlog = await Blog.query().select(['name','date','description']).fetch();
      return response.status(200).json({
        message:"display record",
        data :allBlog
      });
    }catch(error){
      console.log(error);
      throw error;
    }
  }

  async displayuserblog ({ request, response, view ,auth}) {
    try{
      const user = await auth.getUser();

      const userBlog = await Blog.query().select(['id','name','date','description']).where('user_id',user.id).fetch();
      return response.status(200).json({
        message:"display record",
        data :userBlog
      });
    }catch(error){
      console.log(error);
      throw error;
    }
  }

  async updateuserblog ({ request, response, view ,auth}) {
    try{   
      const rules = {
        name: 'required|min:3',
        date: 'required',
        description:'required'
      }

      const validation = await validate(request.all(), rules)

      if (validation.fails()) {
        return validation.messages()
      }

      const{name,date,description} = await request.post();
      
      const user = await auth.getUser();
      
      const userBlogupdate = await Blog.update({
        user_id:user.id,
        name:name,
        date:date,
        description:description
      });
      return response.status(200).json({
        message:"display record",
        data :userBlogupdate
      });    
    }catch(error){
      console.log(error);
      throw error;
    }
  }

  async updateuserblog ({ request, response, view ,auth,params}) {
    try{   
      const rules = {
        name: 'required|min:3',
        description:'required'
      }
      const validation = await validate(request.all(), rules)
      if (validation.fails()) {
        return validation.messages()
      }
       const blogId =  params.id;
       
      //  return response.send(blogId);

      const{name,date,description} = await request.post();
      const user = await auth.getUser();
      
      const userBlogcreate = await Blog.query().where('id',blogId).where('user_id',user.id).update({
        user_id:user.id,
        name:name,
        date:date,
        description:description
      });
      return response.status(200).json({
        message:"record update successfull",
        data :userBlogcreate
      });
    
    }catch(error){
      console.log(error);
      throw error;
    }
  }


  async deleteuserblog ({ request, response, view ,auth,params}) {
    try{   
      
      const user = await auth.getUser();

      const blogId =  params.id;
    const   deletebloag = await Blog.query().where('id',blogId).where('user_id',user.id).delete();
      return response.status(200).json({
        message:"record delete successfull",
      });
    
    }catch(error){
      console.log(error);
      throw error;
    }
  }



}

module.exports = BlogController
